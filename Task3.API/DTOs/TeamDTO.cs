﻿using System;

namespace Task3.API.DTOs
{
	public class TeamDTO
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public DateTime? Created_At { get; set; }
	}
}
