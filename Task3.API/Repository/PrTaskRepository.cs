﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Task3.API.Data;
using Task3.API.DTOs;
using Task3.API.Models;
using Newtonsoft.Json;

namespace Task3.API.Repository
{
	public class ATasP
	{
		public int Count { get; set; }
		public string Name { get; set; }
	}

	public class PrTaskRepository:IPrTaskRepository
	{
		private IDataSource _context;
		private readonly IConfiguration _config;

		public PrTaskRepository(IDataSource source, IConfiguration configuration)
		{
			_context = source;
			_config = configuration;
		}

		//ex 1
		public IDictionary<string, int> GetTasksCount(int author) =>

			(from project in _context.Projects
			 join tasks in _context.Tasks
			 on project.Id equals tasks.Project_Id
			 where project.Author_Id == author
			 group project.Tasks by project.Name)
			 .ToDictionary(pr => pr.Key, pr => pr.Count());

		//ex 2
		public List<Task> GetTasks(int user_Id) =>
			 _context.Tasks.Where(task => task.Performer_Id == user_Id && task.Name.Length < 45).ToList();

		//ex 3
		public List<string> GetFinishedTasks(int user_Id) =>
			_context.Tasks.Where(task => task.Performer_Id == user_Id && task.Finished_At.Year == 2019)
			.Select(task => $"Task Id: {task.Id}, Task name: {task.Name}")
			.ToList();

		//ex 4
		public IDictionary<string, List<User>> GetTeamDefinition() =>

			(from team in _context.Teams
			 join user in _context.Users
			 on team.Id equals user.Team_Id
			 where _context.Users.All(user => (DateTime.Now.Year - user.Birthday.Year) > 12)
			 group user by team)
			 .ToDictionary(d => $"Company id: {d.Key.Id}, Company name: {d.Key.Name}", d =>
			 (
				 d.OrderByDescending(u => u.Registered_At).ToList()
			 ));


		//ex 5
		public IDictionary<User, List<Task>> GetUserTasks() =>

			(from task in _context.Tasks
			 join user in _context.Users
			 on task.Performer_Id equals user.Id
			 group task by user)
			 .OrderBy(k => k.Key.First_Name)
			 .ToDictionary(d => d.Key, d =>
			 (
				 d.OrderByDescending(t => t.Name.Length).ToList()
			 ));

		//ex 6
		public Task6DTO GetUserDefinition(int user_id) =>

		(from user in _context.Users
		 join project in _context.Projects
		 on user.Id equals project.Author_Id
		 join task in _context.Tasks
		 on project.Id equals task.Project_Id
		 let lastPr = _context.Projects.Where(p => p.Author_Id == user_id).OrderBy(p => p.Created_At).LastOrDefault()

		 where user.Id == user_id
		 select new Task6DTO
		 (
			 _context.Users.SingleOrDefault(u => u.Id == user_id),
			 lastPr,
			 _context.Tasks.Count(t=>t.Project_Id == lastPr.Id),
			 _context.Tasks.Count(t => t.Finished_At > DateTime.Now && t.Performer_Id == user_id),
			 _context.Tasks.Where(t => t.Performer_Id == user_id).OrderBy(t => t.Finished_At - t.Created_At).LastOrDefault()
		 )).FirstOrDefault();

		//ex 7
		public Task7DTO GetProjectDefinition(int project_id) =>

		(from team in _context.Teams
		 join project in _context.Projects
		 on team.Id equals project.Team_Id
		 join task in _context.Tasks
		 on project.Id equals task.Project_Id
		 let tasks = _context.Tasks.Count(t => t.Project_Id == project_id)
		 let targetProject = _context.Projects
			.Where(p => p.Description.Length > 25 && tasks < 3)
			.SingleOrDefault(p => p.Id == project_id)//check if currentt Project satisfy conditions
		 where project.Id == project_id
		 select new Task7DTO(
		  project,
		_context.Tasks.Where(t => t.Project_Id == project_id).OrderBy(ts => ts.Description.Length).LastOrDefault(),
		_context.Tasks.Where(t => t.Project_Id == project_id).OrderBy(ts => ts.Name.Length).FirstOrDefault(),
		targetProject == null ? 0 : tasks)).FirstOrDefault();//return 0 if do not satisfy

		//rabbitmq data
		public List<LogModel> GetLog()
		{
			try
			{
				List<LogModel> temp = new List<LogModel>();
				using (StreamReader r = File.OpenText(_config.GetSection("FilePath").Value))
				{
					string line;
					while ((line = r.ReadLine()) != null)
					{
						temp.Add(JsonConvert.DeserializeObject<LogModel>(line));
					}
				};
				return temp.OrderByDescending(m => m.WrittenData).ToList();
			}
			catch
			{
				return null;
			}
		}
	}
}
