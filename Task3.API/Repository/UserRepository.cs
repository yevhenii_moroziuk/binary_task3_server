﻿using System;
using System.Collections.Generic;
using System.Linq;
using Task3.API.Data;
using Task3.API.Models;

namespace Task3.API.Repository
{
	public class UserRepository : IRepository<User>
	{
		private readonly IDataSource _context;

		public UserRepository(IDataSource source)
		{
			_context = source;
		}

		public void Create(User entity)
		{
			//user has Team FK
			if (_context.Teams.Find(t => t.Id == entity.Team_Id) == null)
				throw new Exception("Invalid team Id");

			_context.Users.Add(entity);
		}

		public void Delete(int id)
		{
			var user = _context.Users.Find(u => u.Id == id);
			_context.Users.Remove(user);
		}

		public User Show(int Id)
		{
			return _context.Users.SingleOrDefault(u => u.Id == Id);
		}

		public List<User> ShowAll()
		{
			return _context.Users;
		}

		public void Update(User entity)
		{
			//user has Team FK
			if (_context.Teams.Find(t => t.Id == entity.Team_Id) == null)
				throw new Exception("Invalid team Id");

			var user = _context.Users.SingleOrDefault(u => u.Id == entity.Id);//find this user in collection
			user = entity;
		}

		public bool IsExists(int id)
		{
			return _context.Users.SingleOrDefault(u => u.Id == id) == null ? false : true;
		}
	}
}
