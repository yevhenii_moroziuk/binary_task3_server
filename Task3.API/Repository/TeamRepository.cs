﻿using System.Collections.Generic;
using System.Linq;
using Task3.API.Data;
using Task3.API.Models;

namespace Task3.API.Repository
{
	public class TeamRepository:IRepository<Team>
	{
		private readonly IDataSource _context;

		public TeamRepository(IDataSource source)
		{
			_context = source;
		}

		public void Create(Team entity)
		{
			_context.Teams.Add(entity);
		}

		public void Delete(int id)
		{
			var team = _context.Teams.Find(u => u.Id == id);
			_context.Teams.Remove(team);
		}

		public Team Show(int Id)
		{
			return _context.Teams.SingleOrDefault(u => u.Id == Id);
		}

		public List<Team> ShowAll()
		{
			return _context.Teams;
		}

		public void Update(Team entity)
		{
			var team = _context.Teams.SingleOrDefault(u => u.Id == entity.Id);
			team = entity;
		}

		public bool IsExists(int id)
		{
			return _context.Teams.SingleOrDefault(u => u.Id == id) == null ? false : true;
		}
	}
}
