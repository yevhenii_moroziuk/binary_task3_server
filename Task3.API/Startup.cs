﻿using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Task3.API.Data;
using Task3.API.Hubs;
using Task3.API.Models;
using Task3.API.Queue;
using Task3.API.Repository;

namespace Taks2.API
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
			services.AddSingleton<IDataSource, DataSource>();

			services.AddAutoMapper();
			services.AddSignalR();
			services.AddScoped<QueueService>();

			services.AddTransient<IRepository<User>, UserRepository>();
			services.AddTransient<IRepository<Team>, TeamRepository>();
			services.AddTransient<IRepository<Task>, TaskRepository>();
			services.AddTransient<IRepository<Project>, ProjectRepository>();
			services.AddTransient<IPrTaskRepository, PrTaskRepository>();
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				app.UseHsts();
			}

			app.UseSignalR(routes =>
			{
				routes.MapHub<MessageHub>("/hubmes");
			});

			app.UseHttpsRedirection();
			app.UseMvc();
		}
	}
}
