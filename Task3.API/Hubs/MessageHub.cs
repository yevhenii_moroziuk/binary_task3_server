﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task3.API.Hubs
{
	public class MessageHub:Hub
	{
		public async Task SendMessage(string message)
		{
			await Clients.All.SendAsync("BroadcastMessage", message);
		}
	}
}
