﻿using AutoMapper;
using Task3.API.DTOs;
using Task3.API.Models;

namespace Task3.API.Mapping
{
	public class AutoMapperProfile : Profile
	{
		public AutoMapperProfile()
		{
			//User
			CreateMap<User, UserShowDTO>();
			CreateMap<User, UserPostDTO>();
			CreateMap<UserPostDTO, User>();
			//Team
			CreateMap<Team, TeamDTO>();
			CreateMap<TeamDTO, Team>();
			//Task
			CreateMap<Task, TaskDTO>();
			CreateMap<TaskDTO, Task>();
			//Project
			CreateMap<Project, ProjectDTO>();
			CreateMap<ProjectDTO, Project>();
		}
	}
}
