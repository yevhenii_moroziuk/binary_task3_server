﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Task3.API.DTOs;
using Task3.API.Models;
using Task3.API.Repository;

namespace Task3.API.Controllers
{
	[Route("api/teams")]
    [ApiController]
    public class TeamController : ControllerBase
    {
		private readonly IRepository<Team> _repository;
		private readonly IMapper _mapper;

		public TeamController(IRepository<Team> repository, IMapper mapper)
		{
			_repository = repository;
			_mapper = mapper;
		}

		[HttpGet]
		public IActionResult GetTeams()
		{
			var users = _repository.ShowAll();
			return Ok(_mapper.Map<IEnumerable<TeamDTO>>(users));
		}

		[HttpGet("{id}")]
		public IActionResult GetTeam(int id)
		{
			if (!_repository.IsExists(id))
				return NotFound();

			var team = _repository.Show(id);

			return Ok(_mapper.Map<TeamDTO>(team));
		}

		[HttpPost]
		public IActionResult PostTeam(TeamDTO teamDTO)
		{
			if (!ModelState.IsValid)
				return BadRequest();

			if (_repository.IsExists(teamDTO.Id))
				return BadRequest();

			var team = _mapper.Map<Team>(teamDTO);

			_repository.Create(team);
			return Created("api/teams", team);

		}

		[HttpPut]
		public IActionResult EditTeam(Team entity)
		{
			if (!ModelState.IsValid)
				return BadRequest("Invalid team id");

			if (!_repository.IsExists(entity.Id))
				return NotFound();

			_repository.Update(entity);
			return Ok($"edited: {entity.ToString()}");
		}

		[HttpDelete("{id}")]
		public IActionResult DeleteTeam(int id)
		{
			if (!_repository.IsExists(id))
				return BadRequest();

			_repository.Delete(id);

			return Ok("deleted");
		}
	}
}