﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Task3.API.DTOs;
using Task3.API.Models;
using Task3.API.Repository;

namespace Task3.API.Controllers
{
	[Route("api/users")]
    [ApiController]
    public class UserController : ControllerBase
    {
		private readonly IRepository<User> _repository;
		private readonly IMapper _mapper;

		public UserController(IRepository<User> repository, IMapper mapper)
		{
			_repository = repository;
			_mapper = mapper;
		}

		[HttpGet]
		public IActionResult GetUsers()
		{
			var users = _repository.ShowAll();
			return Ok(_mapper.Map<IEnumerable<UserShowDTO>>(users));
		}

		[HttpGet("{id}")]
		public IActionResult GetUser(int id)
		{
			if (!_repository.IsExists(id))
				return NotFound();

			var user = _repository.Show(id);

			return Ok(_mapper.Map<UserShowDTO>(user));
		}

		[HttpPost]
		public IActionResult PostUser(UserPostDTO userDTO)//(UserDTO userDTO)
		{
			if (!ModelState.IsValid)
				return BadRequest();

			if (_repository.IsExists(userDTO.Id))
				return BadRequest();

			var user = _mapper.Map<User>(userDTO);
			user.Registered_At = DateTime.Now;

			try
			{
				_repository.Create(user);
				return Created("api/user", user);
			}
			catch
			{
				//throw new Exception("Invalid team Id");
				return BadRequest("Invalid team id");
			}
		}

		[HttpPut]
		public IActionResult EditUser(User entity)
		{
			if (!ModelState.IsValid)
				return BadRequest("Invalid user id");

			if (!_repository.IsExists(entity.Id))
				return NotFound();

			try
			{
				_repository.Update(entity);
				return Ok($"edited: {entity.ToString()}");
			}
			catch
			{
				return BadRequest("Invalid team id");
			}
		}

		[HttpDelete("{id}")]
		public IActionResult DeleteUser(int id)
		{
			if (!_repository.IsExists(id))
				return BadRequest();

			_repository.Delete(id);

			return Ok("deleted");
		}
	}
}