﻿using System.Collections.Generic;
using Task3.API.Data.DataSeed;
using Task3.API.Models;

namespace Task3.API.Data
{
	public class DataSource:IDataSource
	{
		public List<Project> Projects { get; set; }
		public List<User> Users { get; set; }
		public List<Task> Tasks { get; set; }
		public List<Team> Teams { get; set; }

		public DataSource()
		{
			Projects = Seed.SeedData<Project>("projects");
			Users = Seed.SeedData<User>("users");
			Tasks = Seed.SeedData<Task>("tasks");
			Teams = Seed.SeedData<Team>("teams");
		}
	}
}
