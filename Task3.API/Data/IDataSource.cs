﻿using System.Collections.Generic;
using Task3.API.Models;

namespace Task3.API.Data
{
	public interface IDataSource
	{
		List<Project> Projects { get; set; }
		List<User> Users { get; set; }
		List<Task> Tasks { get; set; }
		List<Team> Teams { get; set; }
	}
}
