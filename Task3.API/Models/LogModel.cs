﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Task3.API.Models
{
	public class LogModel
	{
		public DateTime WrittenData { get; set; }
		public string Message { get; set; }
	}
}
