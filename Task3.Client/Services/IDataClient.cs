﻿using System.Threading.Tasks;

namespace Task3.Client.Services
{
	public interface IDataClient
	{
		Task<T> GetAsync<T>(string specificAddres);
	}
}
