﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;
using Task3.Worker.Services;

namespace Task3.Worker
{
	class Program
	{
		static void Main(string[] args)
		{
			MessageService s = new MessageService();
			s.Run();

			Console.ReadLine();
			s.Dispose();
		}
	}
}
